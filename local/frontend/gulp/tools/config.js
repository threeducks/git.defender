"use strict";

var path = require("path");
var pathSrc = path.dirname(path.dirname(__dirname));

var srcDir = pathSrc + "/src";
var buildDir = pathSrc + "/build";

module.exports = {

    other: {
        src: srcDir,
        build: buildDir
    },

    pug: {
        index: 100,
        add: false,
        name: "markup:pug",
        path: "./gulp/tasks/pug.js",
        src: srcDir + "/markup/*.pug",
        watch: srcDir + "/markup/**/**/**/*.pug",
        build: buildDir + "/markup/"
    },

    html: {
        index: 100,
        add: true,
        name: "markup:html",
        path: "./gulp/tasks/html.js",
        src: srcDir + "/markup/*.html",
        watch: srcDir + "/markup/**/**/**/*.html",
        build: buildDir + "/markup/"
    },

    stylus: {
        index: 20,
        add: false,
        name: "style:stylus",
        path: "./gulp/tasks/style.js",
        src: srcDir + "/styles/stylus/*.styl",
        watch: srcDir + "/styles/stylus/**/**/*.styl",
        build: buildDir + "/css/"
    },

    sass: {
        index: 20,
        add: true,
        name: "style:sass_or_scss",
        path: "./gulp/tasks/style.js",
        src: srcDir + "/styles/sass/*.{scss,sass}",
        watch: srcDir + "/styles/sass/**/**/**/*.{scss,sass}",
        build: buildDir + "/css/"
    },

    javascript: {
        index: 21,
        add: true,
        name: "javascript",
        path: "./gulp/tasks/javascript.js",
        src: srcDir + "/js/*.js",
        watch: srcDir + "/js/*.js",
        build: buildDir + "/js/"
    },

    javascriptWebpack: {
        index: 21,
        add: false,
        name: "javascript:webpack",
        path: "./gulp/tasks/javascript-webpack.js",
        config: {
            watch: true,
            context: srcDir + '/jswebpack',
            entry: {
                Francysk: './francysk.js'
            },
            output: {
                path: buildDir + '/jswebpack',
                filename: '[name].bundle.js',
                library: '[name]'
            },
            module: {
                rules: [{
                    test: /\.js$/,
                    exclude: [/node_modules/],
                    use: [{
                        loader: "babel-loader",
                        options: {
                            presets: ["es2015-loose", "react"],
                            plugins: ["transform-class-properties"]
                        }
                    }]
                }]
            }
        }
    },

    assets: {
        index: 22,
        add: true,
        name: "assets",
        path: "./gulp/tasks/assets.js",
        src: srcDir + "/assets/**/**/*",
        watch: srcDir + "/assets/**/**/*",
        build: buildDir + '/'
    },

    sprite: {
        index: 0,
        add: true,
        name: 'sprite',
        path: './gulp/tasks/sprite.js',
        src: srcDir + "/sprite/*",
        retina: {
            retinaSrcFilter: srcDir + '/sprite/*@2x.*'
        },
        build: {
            css: srcDir + '/styles/sass/sprite/',
            img: buildDir + '/img/'
        }
    }

};