"use strict";

module.exports = function(defaultKey, keys){
    var data = {};
    var node_env = process.env.NODE_ENV || defaultKey;
    var node_env_keys = node_env.split(" ");

    for (var i = 0, key; i < keys.length; i++){
        key = keys[i];
        for (var n = 0, nKey; n < node_env_keys.length; n++){
            nKey = node_env_keys[n];
            if (key === nKey){
                data[key] = true;
                break;
            } else {
                data[key] = false;
            }
        }
    }

    return data;
};