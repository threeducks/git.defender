"use strict";

var $ = require("gulp-load-plugins")();
var gulp = require("gulp");
var spritesmith = require("gulp.spritesmith");
var buffer = require("vinyl-buffer");
var mergeStream = require("merge-stream");

module.exports = function(opt){
    var env = opt.env;
    var to = opt.taskOptions;

    return function(callback){
        var spriteData = gulp.src(opt.src)
            .pipe(spritesmith({
                imgPath: "../img/sprite@1x.png",
                imgName: "sprite@1x.png",
                cssName: "sprite.scss",
                padding: 15,

                retinaSrcFilter: to.retina.retinaSrcFilter,
                retinaImgName: "sprite@2x.png",
                retinaImgPath: "../img/sprite@2x.png"
            }));

        var imgStream = spriteData.img
            .pipe(buffer())
            .pipe($.if(env.production || env.git, $.imagemin()))
            .pipe(gulp.dest(opt.build.img));

        var cssStream = spriteData.css
            .pipe(gulp.dest(opt.build.css));

        return mergeStream(imgStream, cssStream);
    };

};