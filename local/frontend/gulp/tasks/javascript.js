"use strict";

var $ = require("gulp-load-plugins")();
var gulp = require("gulp");

module.exports = function(opt){
    var env = opt.env;

    return function(callback){
        return gulp.src(opt.src)
            .pipe($.plumber({
                errorHandler: $.notify.onError(function(error){
                    return {
                        title: opt.name,
                        message: error.message
                    };
                })
            }))
            .pipe($.if(env.sourcemaps, $.sourcemaps.init()))
            .pipe($.babel({
                presets: ["es2015"]
            }))
            .pipe($.if(env.production, $.uglify()))
            .pipe($.if(env.sourcemaps, $.sourcemaps.write()))
            .pipe(gulp.dest(opt.build));
    };

};