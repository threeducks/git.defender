"use strict";

var $ = require("gulp-load-plugins")();
var gulp = require("gulp");
var path = require('path');

module.exports = function(opt){
    var env = opt.env;

    var processors = [];

    processors.push(
        require("autoprefixer")({
            browsers: ["last 45 versions"],
            supports: false
        })
    );

    if (env.production){
        processors.push(
            require("cssnano")({
                zindex: false
            })
        );
    }

    return function(callback){
        return gulp.src(opt.src)
            .pipe($.plumber({
                errorHandler: $.notify.onError(function(error){
                    return {
                        title: opt.name,
                        message: error.message
                    };
                })
            }))
            .pipe($.if(env.sourcemaps, $.sourcemaps.init()))
            .pipe($.if(function(file){
                var ext = path.extname(file.path);
                return ext === '.styl';
            }, $.stylus()))
            .pipe($.if(function(file){
                var ext = path.extname(file.path);
                return ext === '.scss' || ext === '.sass';
            }, $.sass({
                outputStyle: 'nested'
            })))
            .pipe($.postcss(processors))
            .pipe($.if(env.sourcemaps, $.sourcemaps.write()))
            .pipe(gulp.dest(opt.build));
    };

};