"use strict";

var $ = require("gulp-load-plugins")();
var gulp = require("gulp");
var webpack = require("webpack");
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = function(opt){
    var env = opt.env;

    return function(callback){

        var config = opt.taskOptions.config;

        config.plugins = [];

        if (env.production){

            config.plugins.push( new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }) );

            config.plugins.push( new webpack.optimize.UglifyJsPlugin() );

        }

        if (env.git || env.production){
            config.watch = false;
        } else {
            // config.devtool = "source-map";
        }

        webpack(config, function(err, stats){
            if (err) throw new gutil.PluginError("webpack", err);

            $.util.log("[webpack]", stats.toString({
                // output options
            }));

            callback();
        });

    };

};