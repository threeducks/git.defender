"use strict";

var $ = require("gulp-load-plugins")({
    rename: {
        'gulp-file-include': 'fileinclude'
    }
});
var gulp = require("gulp");

module.exports = function(opt){
    var env = opt.env;

    return function(callback){
        return gulp.src(opt.src)
            .pipe($.plumber({
                errorHandler: $.notify.onError(function(error){
                    return {
                        title: opt.name,
                        message: error.message
                    };
                })
            }))
            .pipe($.fileinclude({
                prefix: "@@",
                basepath: "@file",
                indent: true
            }))
            .pipe(gulp.dest(opt.build));
    };

};