"use strict";

var $ = require("gulp-load-plugins")();
var gulp = require("gulp");
var path = require("path");

module.exports = function(opt){
    var env = opt.env;

    return function(callback){
        return gulp.src(opt.src)
            .pipe($.plumber({
                errorHandler: $.notify.onError(function(error){
                    return {
                        title: opt.name,
                        message: error.message
                    };
                })
            }))
            .pipe($.if(function(file){
                var extname = path.extname(file.path);
                var imagesExt = ['.jpg', '.jpeg', '.png', '.gif', '.bmp'];
                var status = false;
                if (imagesExt.indexOf(extname) >= 0){
                    status = true;
                }
                return (env.production || env.git) && status;
            }, $.imagemin()))
            .pipe(gulp.dest(opt.build));
    };

};