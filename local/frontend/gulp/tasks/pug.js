"use strict";

var $ = require("gulp-load-plugins")();
var gulp = require("gulp");

module.exports = function(opt){
    var env = opt.env;

    return function(callback){
        return gulp.src(opt.src)
            .pipe($.plumber({
                errorHandler: $.notify.onError(function(error){
                    return {
                        title: opt.name,
                        message: error.message
                    };
                })
            }))
            .pipe($.pug({
                pretty: true
            }))
            .pipe(gulp.dest(opt.build));
    };

};