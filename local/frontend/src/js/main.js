$(function(){

    var window = $(window);
    var App = {

        init: function(){
            this.scrollTo();
            this.gallery();
            this.location();
            this.item();
        },

        initOnLoad: function(){},

        initOnScroll: function(){},

        item: function(){

            $(document).off("click.DefenderItem").on("click.DefenderItem", ".js-item", function(evt){
                evt.preventDefault();
                var self = $(this);
                var index = self.attr("data-index") || 0;
                var gallery = self.attr("data-gallery");
                var galleryElement = $(gallery);
                var slickGallery = $(".js-gallery-init-gl", galleryElement);

                if (galleryElement.length && slickGallery.length){
                    var slick = slickGallery.slick("getSlick");

                    if (galleryElement.hasClass("open")){
                        galleryElement.removeClass("open");
                    } else {
                        slick.goTo(index);
                        galleryElement.addClass("open");
                        slick.setPosition();
                    }
                }
            });

            $(document).off("click.DefenderItemBg").on("click.DefenderItemBg", ".gl__bg", function(){
                var self = $(this);
                self.closest(".gl").removeClass("open");
            });

        },

        location: function(){

            $(document).off("click.DefenderLocation").on("click.DefenderLocation", ".js-location", function(evt){
                evt.preventDefault();
                var self = $(this);
                var id = self.attr("data-id");
                var el = $(id);
                if (el.length){
                    if (el.hasClass("open")){
                        el.closest('.list-gallery-fixed').removeClass("open");
                        el.removeClass("open");
                    } else {
                        el.closest('.list-gallery-fixed').find(".list-gallery__item").removeClass("open");
                        el.addClass("open");
                        el.closest('.list-gallery-fixed').addClass("open");
                    }
                }
            });

            $(document).off("click.DefenderLocationBg").on("click.DefenderLocationBg", ".list-gallery-bg", function(){
                var self = $(this);
                self.closest(".list-gallery-fixed").removeClass("open");
                self.closest(".list-gallery-fixed").find(".list-gallery__item").removeClass("open");
            });

        },

        gallery: function(){

            var galleryJq = $(".js-gallery-init");
            if (!galleryJq.length)
                return;

            galleryJq.each(function(){
                var gallery = $(this);
                var config = {};
                var prevAttr, nextAttr, prev, next;

                if (gallery.hasClass("js-gallery-init-big")){
                    prevAttr = gallery.attr("data-prev");
                    nextAttr = gallery.attr("data-next");
                    if (prevAttr && nextAttr){
                        prev = $(prevAttr);
                        next = $(nextAttr);

                        gallery.on("init", function(e, slick){
                            var count = slick.slideCount;
                            if (count > 1){
                                prev.removeClass("hide").off("click.DefenderSlickBigGallery").on("click.DefenderSlickBigGallery", function(){
                                    slick.prev();
                                });

                                next.removeClass("hide").off("click.DefenderSlickBigGallery").on("click.DefenderSlickBigGallery", function(){
                                    slick.next();
                                });
                            }
                        });

                        gallery.on("beforeChange", function(e, slick, curSlide, nextSlide){
                            var slides = slick.$slides;
                            slides.each(function(){
                                var self = $(this);
                                var iframe = $("iframe", self);
                                iframe.appendTo(self);
                            });
                        });
                    }

                    config = {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        fade: true,
                        dots: false,
                        arrows: false,
                        accessibility: false,
                        infinite: false,
                        swipe: false
                    };

                } else if (gallery.hasClass("js-gallery-init-thumbs")){

                    config = {
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        fade: false,
                        dots: false,
                        arrows: false,
                        accessibility: false,
                        infinite: false,
                        swipe: true,
                        responsive: [
                            {
                                breakpoint: 992,
                                settings: {
                                    slidesToShow: 4
                                }
                            },{
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 3
                                }
                            },{
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 2
                                }
                            },{
                                breakpoint: 380,
                                settings: {
                                    slidesToShow: 1
                                }
                            }
                        ]
                    };

                } else if (gallery.hasClass("js-gallery-init-gl")){

                    prevAttr = gallery.attr("data-prev");
                    nextAttr = gallery.attr("data-next");
                    if (prevAttr && nextAttr) {
                        prev = $(prevAttr);
                        next = $(nextAttr);

                        gallery.on("init", function (e, slick) {
                            var count = slick.slideCount;
                            if (count > 1) {
                                prev.removeClass("hide").off("click.DefenderSlickBigGallery").on("click.DefenderSlickBigGallery", function () {
                                    slick.prev();
                                });

                                next.removeClass("hide").off("click.DefenderSlickBigGallery").on("click.DefenderSlickBigGallery", function () {
                                    slick.next();
                                });
                            }
                        });
                    }

                    config = {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        fade: true,
                        dots: false,
                        arrows: false,
                        accessibility: false,
                        infinite: false,
                        swipe: false
                    };

                }

                gallery.slick(config);
            });


        },

        scrollTo: function(options, complete){

            function getOptions(data){

                if (!('to' in data)) return null;

                data.to = data.to;
                data.duration = +data.duration || 1000;
                data.offsetY = +data.offsetY || 0;

                var firstChar = data.to.charAt(0);
                data.position = 0;
                switch(firstChar){
                    case '#':
                        var toScroll = $(data.to);
                        if (toScroll.length){
                            var offset = toScroll.offset();
                            data.position = offset.top;
                        }
                        break;
                    default:
                        var reg = new RegExp('^\\d+(?:\\.\\d+)?$', '');
                        if (reg.test(data.to)){
                            data.position = data.to;
                        } else {
                            return;
                        }
                }

                data.position -= data.offsetY;

                return data;
            }

            if (options){

                options = getOptions( options );

                $('html, body').stop().animate({
                    scrollTop: options.position
                }, {
                    duration: options.duration,
                    complect: typeof complete == "function" ? complete : function(){}
                });

            } else {
                $(document).on("click.DefenderScrollTo", '.js-scrollTo', function(e){
                    e.preventDefault();
                    var self = $(this),
                        scroll = self.attr('data-options'),
                        scrollData = {};

                    if (scroll === undefined) return;

                    scroll.split(',').forEach(function(value){
                        var scrollExp = value.split(':');
                        var key = $.trim(scrollExp[0]);
                        var val = $.trim(scrollExp[1]);
                        scrollData[key] = val;
                    });

                    var options = getOptions( scrollData );
                    if (options != null){
                        $('html, body').stop().animate({
                            scrollTop: options.position
                        }, {
                            duration: options.duration,
                            complect: typeof complete == "function" ? complete : function(){}
                        });
                    }
                });
            }

        }

    };

    App.init();

    window.on("load.Defender", function(){
        App.initOnLoad();
    });

    window.on("scroll.Defender", function(){
        App.initOnScroll();
    });



});