"use strict";

var gulp = require("gulp");
var del = require("del");
var node_env = require("./gulp/tools/node_env")("dev", ["dev", "git", "sourcemaps", "production"]);
var cg = require("./gulp/tools/config");
var tasks = [];

gulp.task("clean", function(){
    return del(cg.other.build);
});

gulp.task('watch', function(){

    for (var t in cg) {
        if (cg.hasOwnProperty(t)) {
            if (t === "other") continue;

            var task = cg[t];

            task.add && gulp.watch(task.watch, gulp.series(task.name));

        }
    }

});

function lazyLoadTask(add, name, path, index, options, taskOptions){
    options = options || {};
    options.env = node_env;
    options.name = name;
    options.taskOptions = taskOptions;

    if (add){
        gulp.task(name, function(callback){
            var stream = require(path)(options);
            return stream(callback);
        });

        tasks.push({
            name: name,
            index: index
        });
    }

}

for (var t in cg){
    if (cg.hasOwnProperty(t)){
        if (t === "other") continue;

        var task = cg[t];

        lazyLoadTask(
            task.add,
            task.name,
            task.path,
            task.index,
            {
                src: task.src,
                build: task.build
            },
            task
        );

    }
}

tasks.sort(function(a, b){ return a.index > b.index; });
tasks = tasks.map(function(value){ return value.name; });

tasks.unshift("clean");
if (node_env.dev){
    tasks.push("watch");
}

gulp.task("default", gulp.series.apply(gulp, tasks));